# README #
Este repositorio muestra la implementación de la metodología de Behaviour Driven Development en un ejemplo simple: El desarrollo de una libreria con una función para acumular valores con saturación y una función para calcular el valor promedio de un vector. El ejemplo se implementa en Python y utiliza [Behave](https://behave.readthedocs.io/en/latest/) para automaizar las pruebas.
