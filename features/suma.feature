# language: es
# encoding: utf-8

Característica: Disponer de una funcion de suma con saturacion
    Como desarrollador de sistemas embebidos
    Quiero disponer de una libreria sencilla y confiable
    Para poder procesar señales digitales facilmente

Esquema del escenario: Sumas con y sin saturacion
    Cuando efectuo una suma con saturacion de los valores <a> y <b>
    Entonces el resultado de la suma es <resultado>
    Ejemplos:
    |      a     |  b | resultado  |
    |      3     |  5 |     8      |
    | 0x7FFFFFFE |  3 | 0x7FFFFFFF |
    | 0x80000001 | -3 | 0x80000000 |