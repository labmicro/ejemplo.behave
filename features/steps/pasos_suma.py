from behave import *
from suma import *

@when('efectuo una suma con saturacion de los valores {acumulado} y {operando}')
def step_impl(context, acumulado, operando):
    (context.acumulado, context.resultado) = acumular(eval(acumulado), eval(operando))


@then('no hay ningun truncamiento')
def step_impl(context):
    assert context.resultado == 0


@then('hay un truncamiento de numero positivo')
def step_impl(context):
    print(context.acumulado)
    assert context.resultado == 1


@then('hay un truncamiento de numero negativo')
def step_impl(context):
    assert context.resultado == -1


@then('el resultado de la suma es {resultado}')
def step_impl(context, resultado):
    assert context.acumulado == eval(resultado)
