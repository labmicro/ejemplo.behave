from behave import *
from promedio import *


@when('calculo el promedio del vector {vector}')
def step_impl(context, vector):
    (context.promedio, context.resultado) = promedio(eval(vector))


@then('el promedio es valido')
def step_impl(context):
    assert context.resultado == 0


@then('el promedio no es valido')
def step_impl(context):
    assert context.resultado == -1


@then('el resultado del promedio es {resultado}')
def step_impl(context, resultado):
    assert context.promedio == eval(resultado)
