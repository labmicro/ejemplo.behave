from ctypes import c_int32

def acumular(acumulado: int, operando : int) -> (int, int):

    acumulado = c_int32(acumulado)
    operando = c_int32(operando)
    suma = c_int32(acumulado.value + operando.value)

    if (acumulado.value > 0) and (operando.value > 0) and (suma.value < 0):
        return (0x7FFFFFFF, 1)
    elif (acumulado.value < 0) and (operando.value < 0) and (suma.value > 0):
        return (0x80000000, -1)
    else:
        return (suma.value, 0)
