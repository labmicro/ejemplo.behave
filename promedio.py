from suma import acumular

def promedio(valores: tuple) -> (int, int):
    acumulado = 0
    for valor in valores:
        (acumulado, resultado) = acumular(acumulado, valor)
        if resultado != 0:
            return (0, -1)
        resultado = int(acumulado / len(valores))
    return (resultado, 0)